import ListarPaciente from '@/components/Pacientes/ListarPaciente'
import CadastrarPaciente from '@/components/Pacientes/CadastrarPaciente'
import EditarPaciente from '@/components/Pacientes/EditarPaciente'
import ExibirPaciente from '@/components/Pacientes/ExibirPaciente'
import Login from '@/components/Login/Form'
import Logout from '@/components/Login/Logout'
import CadastrarAvaliacaoPostural from '@/components/Avaliacoes/CadastrarAvaliacaoPostural'
import CadastrarAvaliacaoFisioterapeutica from '@/components/Avaliacoes/CadastrarAvaliacaoFisioterapeutica'
import CadastrarEmpresa from '@/components/Empresas/CadastrarEmpresa'
import CadastrarFuncionario from '@/components/Funcionarios/CadastrarFuncionario'
import ListarFuncionarios from '@/components/Funcionarios/ListarFuncionarios'


const routes = [
    {
      path: '/empresa/cadastrar',
      name: 'CadastrarEmpresa',
      component: CadastrarEmpresa
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/logout',
      name: 'Logout',
      component: Logout
    },
    {
      path: '/funcionarios',
      name: 'ListarFuncionarios',
      component: ListarFuncionarios
    },
    {
      path: '/funcionario/cadastrar',
      name: 'CadastrarFuncionario',
      component: CadastrarFuncionario
    },
    {
      path: '/avaliacoes/avaliacaoPostural/cadastrar/:id',
      name: 'CadastrarAvaliacaoPostural',
      component: CadastrarAvaliacaoPostural
    },
    {
      path: '/avaliacoes/avaliacaoFisioterapeutica/cadastrar/:id',
      name: 'CadastrarAvaliacaoFisioterapeutica',
      component: CadastrarAvaliacaoFisioterapeutica
    },
    {
      path: '/pacientes',
      name: 'ListarPaciente',
      component: ListarPaciente
    },
    {
      path: '/pacientes/cadastrar',
      name: 'CadastrarPaciente',
      component: CadastrarPaciente
    },
    {
      path: '/pacientes/:id',
      name: 'ExibirPaciente',
      component: ExibirPaciente
    },
    {
      path: '/pacientes/:id/editar',
      name: 'EditarPaciente',
      component: EditarPaciente
    }
  ]

export default routes
