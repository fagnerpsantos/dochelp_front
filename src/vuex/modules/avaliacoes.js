import Vue from 'vue'

export default {
    state: {
        avaliacaoFisioterapeuticaList: [],
        avaliacaoPosturalList: []
    },
    mutations: {
        updateAvaliacaoFisioterapeuticaList (state, data){
            state.avaliacaoFisioterapeuticaList = data
        },
        updateAvaliacaoPosturalList (state, data){
            state.avaliacaoPosturalList = data
        },
    },
    actions: {
        newAvaliacaoFisioterapeutica(context, data){
            //console.log(data)
            Vue.http.post('api/avaliacoesFisioterapeuticas', data)
        },
        newAvaliacaoPostural(context, data){
            //console.log(data)
            Vue.http.post('api/avaliacoesPosturais', data)
        }
    }
}
