import Vue from 'vue'
export default {
    state: {
        usuarioList: []
    },

mutations: {
    updateUsuarioList (state, data){
        state.usuarioList = data
    }
},
actions: {
    getUsuario (context) {
        Vue.http.get('api/user').then(response => {
            context.commit('updateUsuarioList', response.data)
        })
    }
}
}