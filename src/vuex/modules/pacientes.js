import Vue from 'vue'
export default {
    state: {
        pacienteList: [],
        avaliacaoPosturalList: [],
        avaliacaoFisioterapeuticaList: [],
        pacienteView: {

        }
    },
    mutations: {
        updatePacienteList (state, data){
            state.pacienteList = data
        },
        updatePacienteView (state, data){
            state.pacienteView = data
        },
        updateAvaliacaoPosturalList(state, data){
            state.avaliacaoPosturalList = data
        },
        updateAvaliacaoFisioterapeuticaList(state, data){
            state.avaliacaoFisioterapeuticaList = data
        }
    },
    actions: {
        getPacientes (context) {
            Vue.http.get('api/pacientes').then(response => {
                context.commit('updatePacienteList', response.data)
            })
        },
        getPaciente (context, id) {
            Vue.http.get('api/pacientes/' + id).then(response => {
                context.commit('updatePacienteView', response.data)
            })
        },
        newPaciente(context, data){
            //console.log(data)
            Vue.http.post('api/pacientes', data)
        },
        getAvaliacoesPosturais (context, id) {
            Vue.http.get('api/pacientes/' + id + '/avaliacoesPosturais').then(response => {
                context.commit('updateAvaliacaoPosturalList', response.data)
            })
        },
        getAvaliacoesFisioterapeuticas (context, id) {
            Vue.http.get('api/pacientes/' + id + '/avaliacoesFisioterapeuticas').then(response => {
                context.commit('updateAvaliacaoFisioterapeuticaList', response.data)
            })
        }
    }
}