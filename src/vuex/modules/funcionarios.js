import Vue from 'vue'
export default {
    state: {
        funcionarioList: []
    },

mutations: {
    updateFuncionarioList (state, data){
        state.funcionarioList = data
    }
},
actions: {
    getFuncionarios (context) {
        Vue.http.get('api/funcionarios').then(response => {
            context.commit('updateFuncionarioList', response.data)
        })
    },
    newFuncionario(context, data){
        //console.log(data)
        Vue.http.post('api/funcionarios', data)
    }
}
}