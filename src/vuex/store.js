import paciente from './modules/pacientes'
import usuario from './modules/usuarios'
import avaliacao from './modules/avaliacoes'
import funcionario from './modules/funcionarios'

export default {
    modules: {
        paciente: paciente,
        usuario: usuario,
        avaliacao: avaliacao,
        funcionario: funcionario
    }
}